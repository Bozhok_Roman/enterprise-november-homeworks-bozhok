package com.hillel.task2.beanfactorypostprocessor;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TimesRepeat {
    int value();
}
