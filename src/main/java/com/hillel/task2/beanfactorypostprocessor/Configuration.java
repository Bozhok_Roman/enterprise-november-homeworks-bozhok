package com.hillel.task2.beanfactorypostprocessor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@ComponentScan("com.hillel.task2.beanfactorypostprocessor")
@PropertySource("classpath:prop.properties")
public class Configuration {


    @Bean
    public static PropertySourcesPlaceholderConfigurer configurer() {

        return new PropertySourcesPlaceholderConfigurer();
    }
}
