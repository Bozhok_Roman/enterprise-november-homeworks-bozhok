package com.hillel.task2.beanfactorypostprocessor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
@Component
public class SecondsDelayBPP implements BeanPostProcessor {
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        return bean;
    }
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Field[] fields = bean.getClass().getDeclaredFields();
        for (Field f : fields) {
            SecondsDelay annotation = f.getAnnotation(SecondsDelay.class);
            if (annotation != null) {
                int delay = annotation.value()*1000;
                System.out.println(delay);
                f.setAccessible(true);
                ReflectionUtils.setField(f, bean, delay);

            }
        }
        return bean;

}
}
