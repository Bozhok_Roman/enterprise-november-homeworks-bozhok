package com.hillel.task1;

public interface DAO {
    void save(String value);
    String readAll();
}