package com.hillel.task1.file_db;

import com.hillel.task1.DAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


@Configuration
@Profile("file")
public class FileDbConfig {
    @Bean
    public DAO dao (){

        return new FileDbDao();
    }
}
