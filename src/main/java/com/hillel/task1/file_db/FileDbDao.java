package com.hillel.task1.file_db;


import com.hillel.task1.DAO;

import javax.annotation.PreDestroy;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class FileDbDao implements DAO {

    private String filePath = "test.txt";
    private File file = new File(filePath);
    @Override
    public void save(String value) {

        try {
            Writer writer = new FileWriter(file);
            writer.write(value);
            writer.flush();
            System.out.println("File saved value");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String readAll() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            StringBuilder stringBuilder = new StringBuilder();
            while (true){
                String s = reader.readLine();
                if (s==null){
                    break;
                }
                stringBuilder.append(s);
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
    @PreDestroy
    public void destroy() {
        System.out.println("Destroying  Bean");
        if (!file.delete()) {
            System.err.println("ERROR: failed  to delete file.");
        }
        System.out.println("File exists: " + file.exists());
    }
}
