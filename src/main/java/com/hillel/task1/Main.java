package com.hillel.task1;

import com.hillel.task1.file_db.FileDbConfig;
import com.hillel.task1.in_memory_db.InMemoryConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;


public class Main {
    public static void main(String[] args) {
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(
                InMemoryConfig.class,
                FileDbConfig.class,
                Service.class);
        Service service = ctx.getBean("service", Service.class);
        service.save("test");
        System.out.println(service.readAll());
        ctx.close();
    }
}