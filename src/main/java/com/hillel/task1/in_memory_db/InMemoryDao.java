package com.hillel.task1.in_memory_db;



import com.hillel.task1.DAO;

import java.util.ArrayList;
import java.util.List;

public class InMemoryDao implements DAO {

    private List<String> list = new ArrayList<>();
    @Override
    public void save(String value) {
        list.add(value);
        System.out.println("Add value in memory db");
    }

    @Override
    public String readAll() {
        return list.toString();
    }
}
