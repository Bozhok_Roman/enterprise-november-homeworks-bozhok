package com.hillel.task1.in_memory_db;

import com.hillel.task1.DAO;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;


@Configuration
@Profile("memory")
public class InMemoryConfig {
    @Bean
    public DAO dao (){
        return new InMemoryDao();
    }
}
