package com.hillel.task3.hibernate.employee.dao.impl;

import com.hillel.task3.hibernate.employee.dao.BaseDao;
import com.hillel.task3.hibernate.employee.dao.model.Employee;

import javax.persistence.EntityManager;
import java.util.List;

public class EmployeeDao implements BaseDao<Employee> {

    private final EntityManager entityManager;

    public EmployeeDao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Employee create(Employee employee) {

        this.entityManager.getTransaction().begin();

        this.entityManager.persist(employee);

        this.entityManager.getTransaction().commit();

        return employee;
    }

    @Override
    public void create(List<Employee> list) {
        this.entityManager.getTransaction().begin(); //open
        for (Employee employee : list) {
            this.entityManager.persist(employee);
            this.entityManager.flush(); //Synchronize the persistence context to the underlying database.
            this.entityManager.clear(); //Clear the persistence context, causing all managed entities to become detached.
        }
        this.entityManager.getTransaction().commit(); //commit
    }

    @Override
    public Employee get(long id) {
        return this.entityManager.find(Employee.class, id);
    }

    @Override
    public List<Employee> getAll() {
        return this.entityManager
                .createQuery("select e from Employee e")
                .getResultList();
    }

    @Override
    public Employee update(Employee employee) {
        return this.entityManager.merge(employee);
    }

    @Override
    public boolean delete(long id) {
        int rowCount =
                this.entityManager
                        .createQuery("delete from Employee e where e.id=:id")
                        .setParameter("id", id)
                        .executeUpdate();
        return rowCount != 0;
    }
}
