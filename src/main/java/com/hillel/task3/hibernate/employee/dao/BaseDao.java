package com.hillel.task3.hibernate.employee.dao;


import java.util.List;

public interface BaseDao<T> {

    T create(T t); // C - create

    void create(List<T> list); // C - create

    T get(long id); // R - read

    List<T> getAll(); // R - read

    T update(T t); // U - update

    boolean delete(long id); // D - delete
}