package com.hillel.task3.hibernate.employee;

import com.hillel.task3.hibernate.employee.dao.BaseDao;
import com.hillel.task3.hibernate.employee.dao.impl.EmployeeDao;
import com.hillel.task3.hibernate.employee.dao.model.Employee;
import com.hillel.task3.hibernate.employee.dao.model.Position;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory =
                Persistence.createEntityManagerFactory("employee-exmpl");

        EntityManager em = entityManagerFactory.createEntityManager();

        BaseDao employeeDao = new EmployeeDao(em);

        Employee employee1 = new Employee();
        employee1.setFirstName("Roma");
        employee1.setLastName("Bozhok");
        employee1.setSalary(1000);
        employee1.setPosition(Position.DEVELOPER);


        //        Vehicle created = vehicleDAO.create(vehicle);
        Employee employee2 = new Employee();
        employee2.setFirstName("Roman");
        employee2.setLastName("Bozhokk");
        employee2.setSalary(1500);
        employee2.setPosition(Position.BA);

        employeeDao.create(List.of(employee1, employee2));
        List<Employee> all = employeeDao.getAll();
        System.out.println(all);
    }
}
