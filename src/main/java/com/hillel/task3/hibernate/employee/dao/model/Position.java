package com.hillel.task3.hibernate.employee.dao.model;

public enum Position {
    MANAGER, DEVELOPER, DESIGNER, BA
}
